import math, itertools
from copy import copy
import Part, FreeCAD, math
from FreeCAD import Base
from FreeCAD import ActiveDocument as ad
import DraftVecUtils as vec


def polar2cart(radius, polar, azimuth):
    # kartesian pos

    # radius r
    # polar inclination θ
    # azimuth φ
    # x = r sin θ con φ
    # y = r sin θ sin φ
    # z = r cos θ

    θ = math.radians(polar)
    φ = math.radians(azimuth)
    x = radius * math.sin(θ) * math.cos(φ)
    y = radius * math.sin(θ) * math.sin(φ)
    z = radius * math.cos(θ)
    v = Base.Vector(x, y, z)
    print("r: {} polar: {} azimuth: {} cart: {}".format(radius, polar, azimuth, v))
    return v


def part_of_line(v1, v2, new_length):
    olddist = math.sqrt((v1.x - v2.x) ** 2 + (v1.y - v2.y) ** 2 + (v1.z - v2.z) ** 2)
    x = _part_of_line(v1.x, v2.x, olddist, new_length)
    y = _part_of_line(v1.y, v2.y, olddist, new_length)
    z = _part_of_line(v1.z, v2.z, olddist, new_length)
    return Base.Vector(x, y, z)


def _part_of_line(coodinate1, coordinate2, olddist, newdist):
    # based on https://math.stackexchange.com/questions/175896/finding-a-point-along-a-line-a-certain-distance-away-from-another-point/2109383#2109383
    return coodinate1 - ((newdist * (coodinate1 - coordinate2)) / olddist)


class Timber(object):
    def __init__(self, fbl, fbr, ftr, ftl, bbl, bbr, btr, btl):
        self.fbl = fbl
        self.fbr = fbr
        self.ftr = ftr
        self.ftl = ftl
        self.bbl = bbl
        self.bbr = bbr
        self.btr = btr
        self.btl = btl

        self.faces = {}

        self.face('front', [fbl, fbr, ftr, ftl, fbl])
        self.face('back', [bbl, bbr, btr, btl, bbl])
        self.face('top', [ftl, ftr, btr, btl, ftl])
        self.face('left', [fbl, ftl, btl, bbl, fbl])
        self.face('right', [fbr, ftr, btr, bbr, fbr])
        self.face('bottom', [fbl, fbr, bbr, bbl, fbl])

    def face(self, name, points):
        try:
            print(name)
            p = Part.makePolygon(points)
            self.faces[name] = Part.Face(p)
        except Exception as e:
            Part.show(p)
            raise e from None

    def solid(self):
        fs = []  # Fcad needs a real list not a view
        for f in self.faces.values():
            fs.append(f)
        return Part.makeSolid(Part.makeShell(fs))


class Dome(object):
    def __init__(self, a, b, ra):
        self.a = a
        self.b = b
        self.radius = ra  # aussen
        self.ri = ra - a  # innen

    def part(self, level, segment):
        return DomePart(self, level, segment)

    def all_parts(self):
        for segment in range(15):
            for l in range(4):
                self.part(l, segment).show()

        self.part(2, 15).show()
        self.part(3, 15).show()


class DomePart(object):

    def __init__(self, dome, level, segment):
        self.dome = dome
        self.name = 'l' + str(level) + ' s' + str(segment)
        self.left = segment * 22.5
        self.right = (segment + 1) * 22.5
        self.bottom = 90 - (level * 22.5)
        t = 90 - ((level + 1) * 22.5)
        self.top = max(t, 5)

        self.outside = dome.radius
        self.inside = dome.radius - dome.a
        self.timbers = {}
        print(self.name, self.left, self.right, self.bottom, self.top, self.outside, self.inside)

    def front_bottom_left(self):
        return polar2cart(self.outside, self.bottom, self.left)

    def front_bottom_right(self):
        return polar2cart(self.outside, self.bottom, self.right)

    def front_top_right(self):
        return polar2cart(self.outside, self.top, self.right)

    def front_top_left(self):
        return polar2cart(self.outside, self.top, self.left)

    def back_bottom_left(self):
        return polar2cart(self.inside, self.bottom, self.left)

    def back_bottom_right(self):
        return polar2cart(self.inside, self.bottom, self.right)

    def back_top_right(self):
        return polar2cart(self.inside, self.top, self.right)

    def back_top_left(self):
        return polar2cart(self.inside, self.top, self.left)

    def panel(self):
        return Timber(
            self.front_bottom_left(),
            self.front_bottom_right(),
            self.front_top_right(),
            self.front_top_left(),
            self.back_bottom_left(),
            self.back_bottom_right(),
            self.back_top_right(),
            self.back_top_left()
        )

    def show(self):

        # panel = self.panel()
        # obj = ad.addObject('Part::Feature', self.name + '-panel')
        # obj.Shape = panel.solid()

        oben = self.mk_oben()
        obj = ad.addObject('Part::Feature', self.name + '-oben')
        obj.Shape = oben.solid()

        unten = self.mk_unten()
        obj = ad.addObject('Part::Feature', self.name + '-unten')
        obj.Shape = unten.solid()

        links = self.mk_links()
        obj = ad.addObject('Part::Feature', self.name + '-links')
        obj.Shape = links.solid()

        rechts = self.mk_rechts()
        obj = ad.addObject('Part::Feature', self.name + '-rechts')
        obj.Shape = rechts.solid()

    def timber(self, name):
        if name == "unten":
            return self.timbers[name] or self.mk_unten()
        elif name == "oben":
            return self.timbers[name] or self.mk_oben()

    def mk_unten(self):
        print('unten----')
        fbl = self.front_bottom_left()
        fbr = self.front_bottom_right()
        bbl = self.back_bottom_left()
        bbr = self.back_bottom_right()
        ftl = fbl.add(polar2cart(self.dome.b, self.bottom - 90, self.left))
        ftr = fbr.add(polar2cart(self.dome.b, self.bottom - 90, self.right))
        btl = bbl.add(polar2cart(self.dome.b, self.bottom - 90, self.left))
        btr = bbr.add(polar2cart(self.dome.b, self.bottom - 90, self.right))
        self.timbers['unten'] = Timber(fbl, fbr, ftr, ftl, bbl, bbr, btr, btl)
        return self.timbers['unten']

    def mk_oben(self):
        print('oben----')
        ftl = self.front_top_left()
        ftr = self.front_top_right()
        btl = self.back_top_left()
        btr = self.back_top_right()

        fbl = ftl.sub(polar2cart(self.dome.b, self.top - 90, self.left))
        fbr = ftr.sub(polar2cart(self.dome.b, self.top - 90, self.right))
        bbl = btl.sub(polar2cart(self.dome.b, self.top - 90, self.left))
        bbr = btr.sub(polar2cart(self.dome.b, self.top - 90, self.right))

        self.timbers['oben'] = Timber(fbl, fbr, ftr, ftl, bbl, bbr, btr, btl)
        return self.timbers['oben']

    def mk_links(self):
        print('links----')
        u = self.timber('unten')
        o = self.timber('oben')
        fbl = u.ftl
        bbl = u.btl
        ftl = o.fbl
        btl = o.bbl

        fbr = part_of_line(u.ftl, u.ftr, 40)
        ftr = part_of_line(o.fbl, o.fbr, 40)
        bbr = part_of_line(u.btl, u.btr, 40)
        btr = part_of_line(o.bbl, o.bbr, 40)

        self.timbers['links'] = Timber(fbl, fbr, ftr, ftl, bbl, bbr, btr, btl)
        return self.timbers['links']

    def mk_rechts(self):
        print('rechts----')
        u = self.timber('unten')
        o = self.timber('oben')
        fbr = u.ftr
        bbr = u.btr
        ftr = o.fbr
        btr = o.bbr

        fbl = part_of_line(u.ftr, u.ftl, 40)
        ftl = part_of_line(o.fbr, o.fbl, 40)
        bbl = part_of_line(u.btr, u.btl, 40)
        btl = part_of_line(o.bbr, o.bbl, 40)
        self.timbers['rechts'] = Timber(fbl, fbr, ftr, ftl, bbl, bbr, btr, btl)
        return self.timbers['rechts']


d = Dome(60, 40, 2537)
d.all_parts()
